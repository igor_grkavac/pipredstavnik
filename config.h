#ifndef CONFIG_H
#define CONFIG_H

constexpr int THREAD_MAX = 12;
static constexpr int MAX_SIZE = 8;

static constexpr bool PI_REP = true;


#endif // CONFIG_H
