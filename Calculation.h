#include "matrix.h"
#include <mutex>
#include <set>
#include <vector>

class Calculation {
public:
  int calculation(int n, int &reps_count);
  void thread_calculate();
  void thread_det();

  std::set<std::vector<uint8_t>> reps;
  std::set<std::vector<uint8_t>> new_reps;
  std::set<uint8_t> Dn;

  std::mutex pi_mutex;
  std::mutex new_reps_mutex;
  std::mutex solutions_mutex;
  std::mutex det_mutex;
};
