#include "Calculation.h"
#include "gtest/gtest.h"

// Resenja za    1   2   3   4   5   6   7   8   9   10
//              2   3   5   7  11  19  43  91 227  587
// Tacna resenja
// 2: 0 1

// 3: 0 1 2

// 4: 0 1 2 3

// 5: 0 1 2 3 4 5

// 6: 0 1 2 3 4 5 6 7 8 9

// 7: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 20 24 32
static Calculation c;

TEST(RacunanjeResenja1, Determinanta) {

  int reps_count = 0;
  int Dn = c.calculation(1, reps_count);
  EXPECT_EQ(2, Dn);

  if (PI_REP)
    EXPECT_EQ(2, reps_count);
  else
    EXPECT_EQ(2, reps_count);
}

TEST(RacunanjeResenja2, Determinanta) {

  int reps_count = 0;
  int Dn = c.calculation(2, reps_count);
  EXPECT_EQ(3, Dn);
  if (PI_REP)
    EXPECT_EQ(7, reps_count);
  else
    EXPECT_EQ(3, reps_count);
}

TEST(RacunanjeResenja3, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(3, reps_count);
  EXPECT_EQ(5, Dn);
  if (PI_REP)
    EXPECT_EQ(36, reps_count);
  else
    EXPECT_EQ(12, reps_count);
}

TEST(RacunanjeResenja4, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(4, reps_count);
  EXPECT_EQ(7, Dn);
  if (PI_REP)
    EXPECT_EQ(317, reps_count);
  else
    EXPECT_EQ(39, reps_count);
}

TEST(RacunanjeResenja5, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(5, reps_count);
  EXPECT_EQ(11, Dn);
  if (PI_REP)
    EXPECT_EQ(5624, reps_count);
  else
    EXPECT_EQ(388, reps_count);
}

TEST(RacunanjeResenja6, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(6, reps_count);
  EXPECT_EQ(19, Dn);
  if (PI_REP)
    EXPECT_EQ(251610, reps_count);
  else
    EXPECT_EQ(8102, reps_count);
}

TEST(RacunanjeResenja7, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(7, reps_count);
  EXPECT_EQ(43, Dn);
  if (PI_REP)
    EXPECT_EQ(33642660, reps_count);
  else
    EXPECT_EQ(656103, reps_count);
}

TEST(RacunanjeResenja8, Determinanta) {
  int reps_count = 0;
  int Dn = c.calculation(8, reps_count);
  EXPECT_EQ(91, Dn);
  if (PI_REP)
    EXPECT_EQ(14685630688, reps_count);
  else
    EXPECT_EQ(199727714, reps_count);
}
