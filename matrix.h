#ifndef MATRIX_H
#define MATRIX_H
#include <array>
#include <bits/stdc++.h>
#include <vector>

#include "config.h"

class Matrix {
private:
public:
  int size;
  std::array<bool, MAX_SIZE * MAX_SIZE> mat;

  Matrix();
  Matrix(const std::array<bool, MAX_SIZE * MAX_SIZE> input, int s);
  Matrix(const Matrix &input, int s);
  Matrix(const std::vector<uint8_t> &input, int s);
  Matrix(const Matrix &input);
  Matrix(const std::vector<uint8_t> &input, bool transpose = false);

  const bool &operator()(int row, int column) const {
    return mat[row * size + column];
  }
  bool &operator()(int row, int column) { return mat[row * size + column]; }

  void hex_representation(std::vector<uint8_t> &res, bool transposed = false);
  void next_perm(const Matrix &m, std::vector<uint8_t> &perm);
  int det();
  int det(std::array<bool, MAX_SIZE * MAX_SIZE> &mat, int n);
  void pi_rep(std::vector<uint8_t> &solution);
  void fi_rep(std::vector<uint8_t> &solution);
  void assign_extend_line(std::vector<bool> &extend_line);
  void prepare_perm(std::vector<uint8_t> &perm);
};

#endif // MATRIX_H
