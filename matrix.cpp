#include <bits/stdc++.h>

#include "matrix.h"


// Constructors ///////////////////////////////////////////////////////////////////////
Matrix::Matrix() {}

Matrix::Matrix(const std::array<bool, MAX_SIZE * MAX_SIZE> input, int s) : size(s) {
  mat = input;
}

Matrix::Matrix(const Matrix &input, int s) {
  size = s;
  for (int i = 0; i < size - 1; i++)
    for (int j = 0; j < size - 1; j++) {
      (*this)(i, j) = input(i, j);
    }
}

Matrix::Matrix(const std::vector<uint8_t> &input, int s) {
  size = s;
  int n = size - 1;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
      (*this)(i, j) = (input[i] & (1 << (n - 1 - j)));
    }
}

Matrix::Matrix(const Matrix &input) {
  size = input.size;
  for (int i = 0; i < size; i++)
    for (int j = 0; j < size; j++) {
      (*this)(i, j) = input(i, j);
    }
}

Matrix::Matrix(const std::vector<uint8_t> &input, bool transpose) {
  size = input.size();

  if (transpose) {
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++) {
        (*this)(j, i) = (input[i] & (1 << (size - 1 - j)));
      }
  } else {
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++) {
        (*this)(i, j) = (input[i] & (1 << (size - 1 - j)));
      }
  }
}

///////////////////////////////////////////////////////////////////////////////////////


void Matrix::hex_representation(std::vector<uint8_t> &res, bool transposed) {
  for (int i = 0; i < size; i++) {
    int numb = 0;
    for (int j = 0; j < size; j++)
      if (transposed)
        numb = (numb << 1) + mat[j * size + i];
      else
        numb = (numb << 1) + mat[i * size + j];
    res.push_back(numb);
  }
  sort(res.begin(), res.end());
  return;
}

void Matrix::next_perm(const Matrix &m, std::vector<uint8_t> &perm) {

  size = m.size;
  for (int i = 0; i < size; i++)
    for (int j = 0; j < size; j++) {
      (*this)(j, i) = m(j, perm[i]);
    }
  return;
}

void Matrix::pi_rep(std::vector<uint8_t> &solution) {

  // Prepare vector of permutaions for this matrix
  std::vector<uint8_t> perm;
  this->prepare_perm(perm);
  // Set hex representation into the solution vector
  this->hex_representation(solution);

  std::sort(perm.begin(), perm.end());

  do {
    // Permute columns of matrix with next permutation
    Matrix new_matrix;
    new_matrix.next_perm(*this, perm);

    // Get the hex version of new matrix
    std::vector<uint8_t> potential_solution;
    new_matrix.hex_representation(potential_solution);

    //If new matrix representation is smaller, set it as the new solution
    if (potential_solution < solution)
      solution = potential_solution;

  } while (std::next_permutation(perm.begin(), perm.end()));
}

void Matrix::fi_rep(std::vector<uint8_t> &solution) {

  std::vector<uint8_t> start_solution;

  this->hex_representation(solution);

  start_solution = solution;
  for (int i = -1; i < size; i++) {

    // XOR rows

    std::vector<uint8_t> potential_solution = start_solution;
    //If i == -1, it means we are in the first iteration, when we dont do
    // any XOR, because the first iteration is the identity transformation
    if (i != -1)
      for (int j = 0; j < size; j++) {
        if (j == i)
          continue;
        potential_solution[j] = potential_solution[i] ^ potential_solution[j];
      }

    Matrix res(potential_solution);

    // XOR columns

    for (int k = -1; k < size; k++) {

      auto res2 = res;
      // Similar like for i==-1
      if (k != -1)
        for (int j = 0; j < size; j++) {
          if (j == k)
            continue;
          for (int l = 0; l < size; l++)
            //As we do not have number representation of the columns, we XOR
            // directly elements from matrix
            res2(l, j) = res2(l, j) ^ res2(l, k);
        }

      potential_solution.clear();
      //Get the pi-represent of new matrix
      res2.pi_rep(potential_solution);

      //If new matrix representation is smaller, set it as the new solution
      if (potential_solution < solution)
        solution = potential_solution;
    }
  }
}
int Matrix::det() {

  return det(mat, size);
}

//Basic det calculation
int Matrix::det(std::array<bool, MAX_SIZE * MAX_SIZE> &matrix, int n) {
  int d = 0;

  if (n == 2)
    return matrix[0] * matrix[3] - matrix[1] * matrix[2];
  if (n == 3)
    return (matrix[0] * ((matrix[4] * matrix[8]) - (matrix[7] * matrix[5])) -
            matrix[1] * ((matrix[3] * matrix[8]) - (matrix[6] * matrix[5])) +
            matrix[2] * ((matrix[3] * matrix[7]) - (matrix[6] * matrix[4])));
  else {

    std::array<bool, MAX_SIZE * MAX_SIZE> submat;

    for (int c = 0; c < n; c++) {
      int subi = 0; // submatrix's i value
      for (int i = 1; i < n; i++) {
        int subj = 0;

        for (int j = 0; j < n; j++) {
          if (j == c)
            continue;
          submat[subi * (n - 1) + subj] = matrix[i * n + j];
          subj++;
        }

        subi++;
      }
      d = c % 2 ? (d + matrix[c] * det(submat, n - 1))
                : (d - matrix[c] * det(submat, n - 1));
    }
  }

  return d;
}

//Assign vector of number to the extend strip of the matrix
// extend line = [x,y,b]
void Matrix::assign_extend_line(std::vector<bool> &extend_line) {
  for (int i = 0; i < size; i++) {
    (*this)(i, size - 1) = extend_line[i];
  }
  for (int i = 0; i < size - 1; i++)
    (*this)(size - 1, i) = extend_line[size + i];
}

//Instead of doing a permuation of all columns, we permute only non identical columns
void Matrix::prepare_perm(std::vector<uint8_t> &perm) {

  std::vector<int> res;

  for (int i = 0; i < size; i++) {
    int numb = 0;
    for (int j = 0; j < size; j++)
      numb = (numb << 1) + mat[j * size + i];
    res.push_back(numb);

    int n = i;
    for (int j = 0; j < i; j++) {
      if (res[i] == res[j]) {
        n = j;
        break;
      }
    }
    perm.push_back(n);
  }

  return;
}
