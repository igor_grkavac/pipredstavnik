#include <bits/stdc++.h>
#include <iostream>
#include <mutex>
#include <set>
#include <thread>

#include "Calculation.h"
#include "matrix.h"

const bool operator<(const Matrix &first, const Matrix &second) {
  if (first.size == second.size) {
    for (int i = 0; i < first.size; i++)
      for (int j = 0; j < first.size; j++) {
        if (first(i, j) < second(i, j))
          return true;
        if (first(i, j) > second(i, j))
          return false;
      }
  }

  return first.size < second.size;
}

std::ostream &operator<<(std::ostream &out, const Matrix &m) {

  for (int i = 0; i < m.size; i++) {
    for (int j = 0; j < m.size; j++)
      out << m(i, j) << " ";
  }
  return out;
}

void Calculation::thread_calculate() {

  while (true) {

    pi_mutex.lock();

    // If there is no more reps to extend, we finish
    if (reps.size() == 0) {
      pi_mutex.unlock();
      return;
    }
    std::vector<uint8_t> rep = *reps.begin();
    reps.erase(reps.begin());

    pi_mutex.unlock();

    int n = rep.size() + 1;

    // Extend n-1 size matrix to n size
    Matrix B(rep, (int)n);
    std::vector<bool> extend_line(n + n - 1);

    for (int k = 0; k < n + n; k++) {
      do {
        B.assign_extend_line(extend_line);
        std::vector<uint8_t> res;

        // Find pi or fi rep
        if (PI_REP)
          B.pi_rep(res);
        else
          B.fi_rep(res);

        new_reps_mutex.lock();
        new_reps.insert(res);
        new_reps_mutex.unlock();

      } while (std::next_permutation(extend_line.begin(), extend_line.end()));
      // remove 0 and add 1 to the extension strip
      extend_line.erase(extend_line.begin());
      extend_line.push_back(1);
    }
  }
  return;
}

void Calculation::thread_det() {

  while (true) {
    new_reps_mutex.lock();
    if (new_reps.size() == 0) {
      new_reps_mutex.unlock();
      return;
    }

    Matrix m(*new_reps.begin());
    new_reps.erase(new_reps.begin());

    new_reps_mutex.unlock();

    auto res = m.det();

    det_mutex.lock();
    Dn.insert(abs(res));
    det_mutex.unlock();
  }
}

int Calculation::calculation(int n, int &reps_count) {

  if (n <= 2) {
    reps.insert(std::vector<uint8_t>{0});
    reps.insert(std::vector<uint8_t>{1});
    reps_count = reps.size();
    if (n == 1)
      return 2;
  }

  new_reps.clear();

  std::vector<std::thread> threads;

  for (int i = 0; i < THREAD_MAX; i++)
    threads.push_back(std::thread(&Calculation::thread_calculate, this));

  for (int i = 0; i < THREAD_MAX; i++)
    threads.at(i).join();

  threads.clear();

  reps = new_reps;
  reps_count = reps.size();

  for (int i = 0; i < THREAD_MAX; i++)
    threads.push_back(std::thread(&Calculation::thread_det, this));

  for (int i = 0; i < THREAD_MAX; i++)
    threads.at(i).join();

  return Dn.size() * 2 - 1;
}
